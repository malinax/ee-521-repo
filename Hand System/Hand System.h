/*
 * Hand_System.h
 *
 * Created: 5/13/2014 12:47:51 PM
 *  Author: Cale
 */ 


#ifndef HAND_SYSTEM_H_
#define HAND_SYSTEM_H_

#define F_CPU 16000000  	// tells compiler the freq of your processor

#include <util/delay.h>     // software delay functions
#include <stdlib.h>         // Standard C library
#include <avr/io.h>         //Assuming needed for running AVR chip
#include <stdio.h>
#include <math.h>
#include "MPU6000.h"        //Needed for defining registers found in the MPU6000 chip and specific values used to populate the registers

//Used to printf data to a terminal on computer.
#include "USART.h"   // (see note at top for use instructions)



#define MY_MISO 4  // PB pin 4
#define MY_MOSI 3  // PB pin 3
#define MY_SCK  5  // PB pin 1

/*Have to initialize both the real slave select and PD4 for the SPI system to work.
Read into ATMEGA328P datasheet for more information.*/
#define MY_SS   2  // PB pin 2
#define CHIP_SS 4  // PD pin 4

/*This is the values for the array setup to poll 15 values from the sensors.
These values are averaged and results put into the last row of the array.*/
#define ROWS_OF_ARRAY 7
#define COLUMNS_OF_ARRAY 16

//See table in Register Summary MPU6000 Document for table on the sensitivities
#define ACC_LSB_SENSITIVITY 16384.0
#define GYR_LSB_SENSITIVITY 131.0
//seconds. Assumed because the Sample rate in line 306 is 200Hz, however this should have been increased to 15*200Hz for the mulitiple samples
#define SAMPLE_PERIOD 0.005

//These values simply allow the use of words to designate the columns of the sampling array.
enum {x_axis, y_axis, z_axis};
	
//Defines row 15 of the array as the average of the new data points
#define NUM_DATA_POINTS 15

void MPU6000_init(void);   //Turn on and pole for sensor data

void print_sensor_data(void);
void collect_sensor_data();
void average_sensor_data(void);

void spi_ss_disable(void);           //Initialize Slave Select and allows Master mode to function for SPI
void spi_ss_enable(void);            //Deactivate Slave Select

void SPI_init(void);  //Will setup the Arduino to send directions

void SPI_transmit_master(unsigned char addr, unsigned char data); //ATMEGA sending instructions to MPU6000
unsigned int SPI_transmit_slave(unsigned char addr);              //ATMEGA retrieving for data from a specific register in MPU6000

int acc_data[3];
int acc_avg[3];
int gyr_data[3];
int gyr_avg[3];
int temp_data;
int temp_avg;

double result[3];


/*prints doubles by multiplying by 100 and printing them as integers.
Could not print doubles using the %f in a printf command to PuTTY terminal*/
void print_readings(double j_print);

#endif /* HAND SYSTEM_H_ */