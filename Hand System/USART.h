/*
 * USART.h
 *
 * Created: 5/13/2014 12:16:30 PM
 *  Author: Cale
 */ 


#ifndef USART_H_
#define USART_H_

#include <stdio.h>
#include <avr/io.h>
#define BAUD_PRESCALE 103

void usart_init(uint16_t baudin, uint32_t clk_speedin);
void usart_send(uint8_t data);
uint8_t  usart_recv(void);
uint8_t  usart_isData(void);
int usart_putChar(char c, FILE *stream);

#endif /* USART_H_ */