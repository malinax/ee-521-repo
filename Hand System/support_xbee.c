/*
 * support_xbee.c
 *
 * Created: 5/30/2013 6:08:59 PM
 *  Author: Antonio
 */

#include <stdlib.h>
#include <avr/io.h>
#include <string.h>
#include "support_xbee.h"
#define F_CPU 16000000UL
#include <util/delay.h>

void usart_init(uint16_t baudin, uint32_t clk_speedin);
void usart_send( uint8_t data );
uint8_t usart_recv(void);
uint8_t usart_istheredata(void);
void usart_string_send(char *data);

void Xbee_Init();
void Xbee_WriteLetter(char letter);
char Xbee_CaptureCharacter();
int Xbee_DataOnTheLine();


void Xbee_Init() {
   DDRD |= XB_RX | XB_TX | XB_RTS | XB_RESET;
   PORTD |= 0x00 | (1 << XB_RTS) | (1 << XB_RESET);
   usart_init(BAUD_SET, SPEED_SET);
}

void Xbee_WriteLetter(char letter) {
   //while (!(PINC & (1 << XB_CTS)))
   //;
   usart_send(letter);
}

int Xbee_DataOnTheLine() {
   return usart_istheredata();
}

char Xbee_CaptureCharacter() {
   return usart_recv();
}

void usart_string_send(char *data) {
   int loc;
   for (loc = 0; loc < strlen(data); loc++)
      usart_send(*(data + loc));
}

void usart_init(uint16_t baudin, uint32_t clk_speedin) {
   uint32_t ubrr = (clk_speedin/16UL)/baudin-1;
   UBRR0H = (unsigned char)(ubrr>>8);
   UBRR0L = (unsigned char)ubrr;
   /*UBRR0H = (BAUD_PRESCALE>>8);
   UBRR0L = BAUD_PRESCALE;*/
   /* Enable receiver and transmitter */
   UCSR0B = (1<<RXEN0)|(1<<TXEN0);
   /* Set frame format: 8data, 1stop bit */
   UCSR0C = (1<<USBS0)|(3<<UCSZ00);
   UCSR0A &= ~(1<<U2X0);
}

void usart_send( uint8_t data ) {
   /* Wait for empty transmit buffer */
   while ( !( UCSR0A & (1<<UDRE0)) );
   /* Put data into buffer, sends the data */
   UDR0 = data;
}

uint8_t  usart_recv(void) {
   //Game_UpdateLEDS(0x02);
   /* Wait for data to be received */
   while ( !(UCSR0A & (1<<RXC0)) )
   ;
   /* Get and return received data from buffer */
   return UDR0;
}

uint8_t  usart_istheredata(void) {
   return (UCSR0A & (1<<RXC0));
}