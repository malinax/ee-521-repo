/*
 * Sensor_LED_test.c
 *
 * Created: 5/10/2013 12:05:18 PM
 *  Author: Jessica Hendricks
 */ 

/* Note for using USART: look at USART.h header for an example main function at the end. This header file was originally a C file that would printf "Hello" indefinitely using a terminal program called PuTTY. There is an alternate print function "usart_send();" 
that can be used to directly send ASCII characters but the printf and this other function cannot be used at the SAME TIME in the main function. If you uncomment the main you will see the two "usart_send" functions are still commented out. Hope this helps with use of 
USART library*/
#include "Hand System.h"


int main(void) {
	//Note: for arrays with four values, order is x=0,y=1,z=2,tot=3, three value is: x=0,y=1,z=2
	double Racc[3]; 
	double Racc_normalization;

	double result_normalization;
	double Rgyro[3];
  
	double Rate_Axz=0, Rate_Ayz=0;
	int Rzgyro_sign=0;
	double Axz=0, Ayz=0;

	int i;
	int first_data = 1;
	int wgyro = 10; //any value between 5 and 20 will work and weights the accuracy of gyro as greatern vs. accel.
  
	//Next two lines needed to initialize USART on the ATMEGA328p 
	usart_init(9600, 16000000);
    
	DDRD = 1<<DDD6 | 1<<DDD5|1<<CHIP_SS;         // digital PORTD6,D5 = output and MPU6000 SS output
	DDRB = 1<<MY_MOSI | 1<<MY_SCK | 1<<MY_SS;    // make MOSI, SCK outputs and MISO input
 
	SPI_init();
	MPU6000_init();
   
	while(1) {    
		average_sensor_data();
	   
		//Accelerometer calculations to get values in G's (1G =9.81m/s^2):
		for(i = 0; i < 3; i++) {
			Racc[i] = acc_avg[i] / ACC_LSB_SENSITIVITY;
		}
		  
		//Accel calculations to get the normalized vector:
		Racc_normalization = sqrt((pow(Racc[0],2)+pow(Racc[1],2)+pow(Racc[2],2))); 
		Racc[x_axis] /= Racc_normalization;
		Racc[y_axis] /= Racc_normalization;
		Racc[z_axis] /= Racc_normalization;
	   
	    //this isn't used anywhere
		//Racc_norm[3] = sqrt((pow(Racc_norm[0],2)+pow(Racc_norm[1],2)+pow(Racc_norm[2],2))); // should = 1 or very close
	   
		Racc[x_axis] *= 100;
		Racc[y_axis] *= 100;
		Racc[z_axis] *= 100;
	   
		//Following code works if initial position is holding the chip parallel to ground and circuitry facing up.

		//Moving the circuit straight up, or down will not cause Y inertia to change and it is initially near or at 0.9 as a normalized vector 
		if (80 < Racc[y_axis]) 
		{
			//Z-inertia changing if the circuit is moved straight up or down	   
			if (-10>Racc[z_axis] && Racc[x_axis] <20 && Racc[x_axis]>-20)	
			{
				printf("FORWARD\n");
				_delay_ms(250);
			}	
          
			else if (10<Racc[z_axis] && Racc[x_axis] <20 && Racc[x_axis]>-20)	
			{
				printf("BACKWARD\n");
				_delay_ms(250);
			}
		// This is the value ranges if the circuit remains in the intial position
			else{  //( -20<Raccx && 20>Raccx && Raccz >-10 && Raccz <10)
			printf("STOP\n");
			_delay_ms(250);
			}	
		}	  	  
		/* Change in the x-inertia occurs most accurately when rotating 
		the wrist to the right or left an usually gets close to 90
		However if the rotation is not purely in the x-direction this value can be lot lower which is why the value of 50 was chosen to allow for a large margin of error*/
			else if (Racc[x_axis]<-50){
			printf("RIGHT\n");
			_delay_ms(250);
		}
			else if (Racc[x_axis]>50) {
			printf("LEFT\n");
			_delay_ms(250);
		}
	   
/* WARNING. EVERYTHING BEYOND THIS POINT IN THE FUNCTION IS A COMPLETELY AND TOTAL SHIT SHOW. */
	   
	   
		//if ((Raccx < 0) && (lastaccx <= 0)||(Raccx >= 0) && (lastaccx >= 0)) 	 
	     
		// There are still bugs in the math that follows and it needs to be tested and    edited before being used.
		//Gyroscope calculations:
		
		Rate_Axz = gyr_avg[x_axis] / GYR_LSB_SENSITIVITY;
		Rate_Ayz = gyr_avg[y_axis] / GYR_LSB_SENSITIVITY;
		
		if (first_data) {
			for(i = 0; i < 3; i++) {
				result[i] = Racc[i];
			}
			first_data = 0;
		}
		else {
			if(abs(result[2]) < 0.1) {
				for(i = 0; i < 3; i++) {
					Rgyro[i] = result[i];
				}
			}
			else {
				Axz = atan2(result[0],result[2]) * 180 / M_PI;
				Ayz = atan2(result[1],result[2]) * 180 / M_PI;
				Axz += Rate_Axz*SAMPLE_PERIOD;   //sample_period in seconds
				Ayz += Rate_Ayz*SAMPLE_PERIOD;
		   
				Rzgyro_sign = cos(Axz * M_PI / 180) >= 0 ? 1 : -1;
		   
			   /*
				Rgyro[0] = 1/(sqrt(1+pow(1/(tan(Axz_NEW)),2)*pow(1/(cos(Ayz_NEW)),2)));
				Rgyro[1] = 1/(sqrt(1+pow(1/(tan(Ayz_NEW)),2)*pow(1/(cos(Axz_NEW)),2)));
				Rgyro[2] = Rzgyro_sign*sqrt(1-pow(Rgyro[0],2)-pow(Rgyro[1],2));
			   */
				Rgyro[0] = sin(Axz * M_PI / 180);
				Rgyro[0] /= sqrt(1 + pow(cos(Axz * M_PI / 180), 2) * pow(tan(Ayz * M_PI / 180), 2));
				Rgyro[1] = sin(Ayz * M_PI / 180);
				Rgyro[1] /= sqrt( 1 + pow(cos(Ayz * M_PI / 180), 2) * pow(tan(Axz * M_PI / 180) , 2));
				Rgyro[2] = Rzgyro_sign * sqrt(1 - pow(Rgyro[0], 2) - pow(Rgyro[1], 2));
			}
			
		   
			result[0] = (Racc[0]+Rgyro[0]*wgyro)/(1+wgyro);
			result[1] = (Racc[1]+Rgyro[1]*wgyro)/(1+wgyro);
			result[2] = (Racc[2]+Rgyro[2]*wgyro)/(1+wgyro);
			result_normalization = sqrt((pow(result[0],2)+pow(result[1],2)+pow(result[2],2)));  
		   
			for(i = 0; i < 3; i ++) {
				result[i] /= result_normalization;
				printf("%d", (int) (result[i] * 100));
			}
			
		}
		
		 	 
	}  // end while

	return 0;
}  // end main



void MPU6000_init(void) {
   //resets all values in MPU_6000 to default values. See RM(register map) section 3 for more info
    SPI_transmit_master(MPUREG_PWR_MGMT_1, BIT_H_RESET); 
    _delay_us(100);
	// Auxiliary I2C Supply Selection, set to zero for MPU-6000
    SPI_transmit_master(MPUREG_AUX_VDDIO, 0); 
    _delay_us(100);
	// Wake up device and select GyroZ clock (better performance and at 8kHz). Possible future use for Sleep Mode and use interupts to wake up.
    SPI_transmit_master(MPUREG_PWR_MGMT_1, MPU_CLK_SEL_PLLGYROZ); 
    _delay_us(100);
	// Disable I2C bus (recommended on datasheet)(because just setting  the chip to SPI was too easy)
    SPI_transmit_master(MPUREG_USER_CTRL, BIT_I2C_IF_DIS); 
    _delay_us(100);
   //THis can be sped up // Sample rate = 200Hz    Fsample= 1Khz/(4+1) = 200Hz
   SPI_transmit_master(MPUREG_SMPLRT_DIV, 0x04);     
    _delay_us(100); 
	// filtering speed  42Hz
    SPI_transmit_master(MPUREG_CONFIG, BITS_DLPF_CFG_42HZ);   
    _delay_us(100);
	// Gyro scale 250�/s
    SPI_transmit_master(MPUREG_GYRO_CONFIG, BITS_FS_250DPS);  
    _delay_us(100);
	// Accel scele 2g (g=8192)
    SPI_transmit_master(MPUREG_ACCEL_CONFIG, BITS_FS_2G);           
    _delay_us(100);
    // BIT_MOT_EN set to one allows interupts when motion is detected.
    SPI_transmit_master(MPUREG_INT_ENABLE, BIT_MOT_EN);     
	//considering allowing DATA_READY_IN which will cause an interrupt to occur anytime the data is finished writing to the chip...

}

void spi_ss_disable() {
    //PORTD = 1<<MY_SS;
	PORTD |= 1<<CHIP_SS;
	
	/*Note: Configured as an input. Must remain high for Master to run through operations.
	To avoid SS from interupting opperations, set as output when in master mode*/
}

void spi_ss_enable() {
     PORTD &= ~(1<<CHIP_SS); 
	 //PORTB = 0<<MY_SS; 
/*1. The master bit (MSTR) in the SPI Control Register (SPCR) is cleared and the SPI system becomes a slave. The direction of the pins will be switched (MOSI=input, Miso = user def, SS=input, SCK =input)
2. The SPI Interrupt Flag (SPIF) in the SPI Status Register (SPSR) will be set. If the SPI interrupt and the global interrupts are enabled the interrupt routine will be executed.*/ 
}

void SPI_init(void) {      
   SPCR = (0<<SPIE) |         //No interrupts
   (1<<SPE) |                 //SPI enabled
   (0<<DORD) |             //shifted out LSB
   (1<<MSTR) |             //master

//This bit and the next mean the master will capture on the rising edge and shift on //the falling edge as determined by the MPU6000 PS_documentation
   (0<<CPOL) |             //rising leading edge   
   (0<<CPHA) |             //sample leading edge
      
//clock speed at  F_CPU/16 = 1MHz(001) ...F_CPU/32 = 500kHz(110)...F_CPU/64 = 250kHz(111,010)...F_CPU/128 = 125kHz(011)...
   (1<<SPR1) | (1<<SPR0) ; 

   SPSR = (0<<SPIF) |         //SPI interrupt flag
   (0<<WCOL) |             //Write collision flag
   (0<<SPI2X) ;             //Doubles SPI clock
   spi_ss_enable();
}

void SPI_transmit_master(unsigned char addr, unsigned char data) {   
    spi_ss_enable();              // assert the slave select
	
    SPDR = addr;                  // Start transmission of address, send high byte firs
    while (!(SPSR & (1<<SPIF)));  // Wait (poll) for transmission complete
	SPDR = data;                  // Start transmission of data, send high byte first
    while (!(SPSR & (1<<SPIF)));  // Wait (poll) for transmission complete
	
    spi_ss_disable();             // de-assert the slave select
}

unsigned int SPI_transmit_slave(unsigned char addr) {
    spi_ss_enable();              // assert the slave select
    SPDR = addr;                  // Start transmission of address, send high byte first
    while (!(SPSR & (1<<SPIF)));  // Wait (poll) for transmission complete
    spi_ss_disable();
    return SPDR;                  /* Return Data Register */
}


void collect_sensor_data() {
	  SPI_transmit_slave(MPUREG_ACCEL_XOUT_H | 0x80);
	  acc_data[x_axis] = (SPI_transmit_slave(MPUREG_ACCEL_XOUT_H) << 8) | SPI_transmit_slave(MPUREG_ACCEL_XOUT_L);
	  
	  SPI_transmit_slave(MPUREG_ACCEL_YOUT_H | 0x80);
	  acc_data[y_axis] = (SPI_transmit_slave(MPUREG_ACCEL_YOUT_H) << 8) | SPI_transmit_slave(MPUREG_ACCEL_YOUT_L);  
	  
	  SPI_transmit_slave(MPUREG_ACCEL_ZOUT_H | 0x80);
	  acc_data[z_axis] = (SPI_transmit_slave(MPUREG_ACCEL_ZOUT_H) << 8) | SPI_transmit_slave(MPUREG_ACCEL_ZOUT_L);
	  
	  SPI_transmit_slave(MPUREG_GYRO_XOUT_H | 0x80);
	  gyr_data[x_axis] = (SPI_transmit_slave(MPUREG_GYRO_XOUT_H) << 8) | SPI_transmit_slave(MPUREG_GYRO_XOUT_L);
	  
	  SPI_transmit_slave(MPUREG_GYRO_YOUT_H | 0x80);
	  gyr_data[y_axis] = (SPI_transmit_slave(MPUREG_GYRO_YOUT_H) << 8) | SPI_transmit_slave(MPUREG_GYRO_YOUT_L);
	  
	  SPI_transmit_slave(MPUREG_GYRO_ZOUT_H | 0x80);
	  gyr_data[z_axis] = (SPI_transmit_slave(MPUREG_GYRO_ZOUT_H)) << 8 | SPI_transmit_slave(MPUREG_GYRO_ZOUT_L);
	  
	  SPI_transmit_slave(MPUREG_TEMP_OUT_H | 0x80);
	  temp_data = (SPI_transmit_slave(MPUREG_TEMP_OUT_H) << 8) | SPI_transmit_slave(MPUREG_TEMP_OUT_L);
}

void average_sensor_data() {
	int i, j;
	
	for(i = 0; i < 3; i++) {
		acc_avg[i] = 0;
		gyr_avg[i] = 0;
	}
	temp_avg = 0;
	
	for(i = 0; i < NUM_DATA_POINTS; i++) {
		collect_sensor_data();
		for(j = 0; j < 3; j++) {
			acc_avg[j] += acc_data[j];
			gyr_avg[j] += gyr_data[j];
		}
		temp_avg += temp_data;
	}
	
	for(i = 0; i < 3; i++) {
		acc_avg[i] /= 15;
		gyr_avg[i] /= 15;	
	}
	temp_avg /= 15;
}

void print_readings(double j_print){
    int Racc_int = (j_print)*100;
	printf("%d", Racc_int);
}