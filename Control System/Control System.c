#define F_CPU 16000000 //define internal CLK speed 
#define MOSI 3 // PB pin 3 
#define SCK 5 // PB pin 5 
#define SS 2 // PB pin 2 
#define SS_2 1 // PB pin 1 
#define BAUD_PRESCALE 103

#include <stdlib.h> // Standard C library 
#include <avr/io.h> 
#include <util/delay.h> 
#include <avr/interrupt.h> 

void Initialize_SPI_Master(void) { 
	SPCR = (0<<SPIE) | //No interrupts 
	(1<<SPE) | //SPI enabled 
	(0<<DORD) | //shifted out LSB 
	(1<<MSTR) | //master 
	(0<<CPOL) | //rising leading edge 
	(0<<CPHA) | //sample leading edge 
	(0<<SPR1) | (0<<SPR0) ; //clock speed 
	SPSR = (0<<SPIF) | //SPI interrupt flag 
	(0<<WCOL) | //Write collision flag 
	(0<<SPI2X) ; //Doubles SPI clock 
	PORTB = 1 << SS; // make sure SS is high 
} 

void Transmit_SPI_Master(int Data, int DAC) { 
	if (DAC == 1) {
		PORTB &= ~(1 << SS); //Assert slave select 
		SPDR = ((Data >> 8) & 0xF) | 0x70; //Attach configuration Bits onto MSB 
		while (!(SPSR & (1<<SPIF))); 
		SPDR = 0xFF & Data; 
		while (!(SPSR & (1<<SPIF))); PORTB = 1 << SS; 
	}
	else {
		PORTB &= ~(1 << SS_2); //Assert slave select 
		SPDR = ((Data >> 8) & 0xF) | 0x70; //Attach configuration Bits onto MSB 
		while (!(SPSR & (1<<SPIF))); 
		SPDR = 0xFF & Data; 
		while (!(SPSR & (1<<SPIF))); PORTB = 1 << SS_2;
	}
} 

void usart_init(uint16_t baudin, uint32_t clk_speedin) {
	uint32_t ubrr = (clk_speedin/16UL)/baudin-1;
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/*UBRR0H = (BAUD_PRESCALE>>8);
	UBRR0L = BAUD_PRESCALE;*/
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 1stop bit */
	UCSR0C = (0<<USBS0)|(3<<UCSZ00);
	UCSR0A &= ~(1<<U2X0);
}

/*the send function will put 8bits on the trans line. */
void usart_send( uint16_t data ) {
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = data;
}

/* the receive data function. Note that this a blocking call
Therefore you may not get control back after this is called 
until a much later time. It may be helpfull to use the 
istheredata() function to check before calling this function
	@return 8bit data packet from sender
*/
uint16_t  usart_recv(void) {
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer */
	return UDR0;
}

/* function check to see if there is data to be received
	@return true is there is data ready to be read */
uint16_t  usart_istheredata(void) {
	 return (UCSR0A & (1<<RXC0));
}


int main(void) { 
 
	DDRB = 1<<MOSI | 1<<SCK | 1<<SS | 1<<SS_2; // make MOSI, SCK, SS, and SS_2 outputs

	usart_init(9600, 16000000);
	Initialize_SPI_Master(); 
	sei(); 

	// Initalize wires: 2.5 Volts on both blue and yellow wires 
	Transmit_SPI_Master(0x800, 0x01); // Set Channel Select for Yellow  
	Transmit_SPI_Master(0x800, 0x02); // Set Channel Select for Blue 

	while(1){	
		switch (usart_recv()){   //use test for  
			case 0b00000001:  			//Forward (1)
				// Set Yellow Wire to 2.5 Volts 
				Transmit_SPI_Master(0x800, 0x01); // Set Channel Select for Yellow 

				// Set Blue Wire to 1.2 Volts 
				Transmit_SPI_Master(0x3E8, 0x02); // Set Channel Select for Blue
				break;
			case 0b00000010:			//Forward Right (2)
				// Set Yellow Wire to 3.6 Volts 
				Transmit_SPI_Master(0xBB8, 0x01); 

				// Set Blue Wire to 3.6 Volts 
				Transmit_SPI_Master(0xBB8, 0x02); 
				break;
			case 0b00000011: 			//Right (3) 
				// Set Yellow Wire to 2.5 Volts
				Transmit_SPI_Master(0x800, 0x01); 

				// Set Blue Wire to 3.6 Volts
				Transmit_SPI_Master(0xBB8, 0x02);
				break;
			case 0b00000100:           //Backward Right (4)
				// Set Yellow Wire to 1.2 Volts
				Transmit_SPI_Master(0x3E8, 0x01); 

				// Set Blue Wire to 3.6 Volts 
				Transmit_SPI_Master(0xBB8, 0x02);
				break; 
			case 0b00000101: 			//Backwards (5)
				// Set Yellow Wire to 3.6 Volts
				Transmit_SPI_Master(0xBB8, 0x01); 

				// Set Blue Wire to 2.5 Volts 
				Transmit_SPI_Master(0x800, 0x02);
				break;
			case 0b00000110: 			//Backward Left (6)
				// Set Yellow Wire to 1.2 Volts
				Transmit_SPI_Master(0x3E8, 0x01); 

				// Set Blue Wire to 1.2 Volts
				Transmit_SPI_Master(0x3E8, 0x02);
				break; 
			case 0b00000111: 		//Left (7)
				// Set Yellow Wire to 2.5 Volts 
				Transmit_SPI_Master(0x800, 0x01);

				// Set Blue Wire to 1.2 Volts
			   	Transmit_SPI_Master(0x3E8, 0x02); 
				break;
			case 0b00001000: 			//Forward Left (8)
				// Set Yellow Wire to 3.6 Volts 
				Transmit_SPI_Master(0xBB8, 0x01);

				// Set Blue Wire to 1.2 Volts 
				Transmit_SPI_Master(0x3E8, 0x02);
				break;
			default:
		   		// ERROR: WE HAVE A PROBLEM
				Transmit_SPI_Master(0x800, 0x01); // Set Channel Select for Yellow 
				Transmit_SPI_Master(0x800, 0x02); // Set Channel Select for Blue
				break;                
		} // end switch

	}

}
