/*
 * support_xbee.h
 *
 * Created: 5/30/2013 6:32:23 PM
 *  Author: Antonio
 */ 


#ifndef SUPPORT_XBEE_H_
#define SUPPORT_XBEE_H_

#define BAUD_PRESCALE 103
#define XB_RX 0
#define XB_TX 1
#define XB_RTS 2
#define XB_CTS 3
#define XB_RESET 4

#define SPEED_SET 16000000
#define BAUD_SET 9600

/*
void usart_init(uint16_t baudin, uint32_t clk_speedin);
void usart_send( uint8_t data );
uint8_t  usart_recv(void);
uint8_t  usart_istheredata(void);
void usart_string_send(char *data);
*/

void Xbee_Init();
void Xbee_WriteLetter(char letter);
char Xbee_CaptureCharacter();
int Xbee_DataOnTheLine();


#endif /* SUPPORT_Xbee_H_ */